import re
import math
from math import sqrt
from math import sin
from math import cos
from math import atan
from math import degrees
from math import radians
from random import randint

class Latitude:
	Degrees = 0
	Minutes = 0
	Seconds = 0
	Hemisphere = ''

class Longitude:
	Degrees = 0
	Minutes = 0
	Seconds = 0
	Meridian = ''
	
class coordinatesGraphs:
  
  __latitude = [Latitude(),Latitude()]
  __longitude =  [Longitude(),Longitude()]
  __altitude = 0
  
  __numericalLatitude = [0,0]
  __numericalLongitude = [0,0]
  __numericalAltitude = [0,0]

  __cartesianLatitude = [0,0]
  __cartesianLogintude = [0,0]
  __cartesianAltitude = [0,0]
  
  def addValueCurrent(self, LatDegrees, LatMinutes, LatSeconds, Hemisphere, 
                LongDegress, LongMinutes, LongSeconds, Meridian, Altitude):
    self.__setLatitude(LatDegrees, LatMinutes, LatSeconds, Hemisphere, 0)  
    self.__setLongitude(LongDegress, LongMinutes, LongSeconds, Meridian, 0)
    self.__altitude = Altitude  
  
  def addValueObstacle(self, LatDegrees, LatMinutes, LatSeconds, Hemisphere, 
                LongDegress, LongMinutes, LongSeconds, Meridian, Altitude):
    self.__setLatitude(LatDegrees, LatMinutes, LatSeconds, Hemisphere, 1)  
    self.__setLongitude(LongDegress, LongMinutes, LongSeconds, Meridian, 1)
    self.__altitude = Altitude

  def __setLatitude(self, Degrees, Minutes, Seconds, Hemisphere, index):
    
    if (Degrees < 0):
      raise ValueError("Value of degrees can't be negative.")
      
    if (Minutes < 0):
      raise ValueError("Value of minutes can't be negative.")
      
    if (Seconds < 0):
      raise ValueError("Value of seconds can't be negative.")      
    
    Hemisphere = Hemisphere.upper()
    
    if (Hemisphere == '') or ((Hemisphere != 'S') and (Hemisphere != 'N')) :
       raise ValueError("Argument invalid hemisphere.")
       
    self.__latitude[index].Degrees = Degrees
    self.__latitude[index].Minutes = Minutes
    self.__latitude[index].Seconds = Seconds
    self.__latitude[index].Hemisphere = Hemisphere

  def __setLongitude(self, Degrees, Minutes, Seconds, Meridian, index): 
    
    if (Degrees < 0):
      raise ValueError("Value of degrees can't be negative.")
      
    if (Minutes < 0):
      raise ValueError("Value of minutes can't be negative.")
      
    if (Seconds < 0):
      raise ValueError("Value of seconds can't be negative.")       
    
    Meridian = Meridian.upper()
    
    if (Meridian == '') or ((Meridian != 'W') and (Meridian != 'O')) :
       raise ValueError("Invalid meridian argument.")
       
    self.__longitude[index].Degrees = Degrees
    self.__longitude[index].Minutes = Minutes
    self.__longitude[index].Seconds = Seconds
    self.__longitude[index].Meridian = Meridian  
    
  def GeodesicsToInteger(self):
    
    index = 0 
    
    while index <= 1:
      
      self.__numericalLatitude[index] = self.__latitude[index].Degrees + (self.__latitude[index].Minutes / 60) + (self.__latitude[index].Seconds / 3600)
      
      if self.__latitude[index].Hemisphere == 'S':
        self.__numericalLatitude[index] *= -1
     	
      self.__numericalLongitude[index] = self.__longitude[index].Degrees + (self.__longitude[index].Minutes / 60) + (self.__longitude[index].Seconds / 3600)
     
      if self.__longitude[index].Meridian == 'W':
        self.__numericalLongitude[index] *= -1
      
      self.__numericalAltitude[index] = self.__altitude
      
      index = index + 1; 
    
  def convertGeodesicsToCartesian(self, geodeticLatitude, geodeticLongitude, ellipsoidalAltitude):
	
  	geodeticLatitude = radians(geodeticLatitude)
  	
  	geodeticLongitude = radians(geodeticLongitude)
  	
  	ellipsoidalAltitude = radians(ellipsoidalAltitude)
  	
  	flatteningEllipsoid = 1/298.25
  
  	firstEllipsoidEccentricity = sqrt(flatteningEllipsoid*(2-flatteningEllipsoid))
  
  	semiMajorAxis = 6378160
  	
  	radiusCurvatureFirstVertical = semiMajorAxis/sqrt(1- firstEllipsoidEccentricity**2 * sin(geodeticLatitude)**2) 
  	
  	x = (radiusCurvatureFirstVertical + ellipsoidalAltitude)* cos(geodeticLatitude) * cos(geodeticLongitude)
  	y = (radiusCurvatureFirstVertical + ellipsoidalAltitude)* cos(geodeticLatitude) * sin    (geodeticLongitude)
  	
  	z = (radiusCurvatureFirstVertical * (1 - firstEllipsoidEccentricity**2) + ellipsoidalAltitude) * sin(geodeticLatitude)
  	
  	return (x,y,z)
  	
  def convertCartesianToGeodesics(self, x, y, z):

    flatteningEllipsoid = 1/298.25
    
    firstEllipsoidEccentricity = sqrt((flatteningEllipsoid*(2-flatteningEllipsoid)))
    
    secondEllipsoidEccentricity = sqrt(firstEllipsoidEccentricity**2/(1 - (firstEllipsoidEccentricity**2)))
    
    semiMajorAxis = 6378160 	
    
    semiMinorAxis = 6356774.7199
	
    tan_u = (z / (sqrt((x**2) + (y**2)))) * (semiMajorAxis/semiMinorAxis) 
	
    cos_u = 1 / (sqrt( 1 + tan_u**2 ))
	
    sen_u = tan_u / (sqrt( 1 + tan_u**2 ))
	
    numerator = z + (secondEllipsoidEccentricity ** 2) * semiMinorAxis * (sen_u**3)
	
    denominator = sqrt((x**2)+(y**2)) - (firstEllipsoidEccentricity**2) * semiMajorAxis * (cos_u**3)
    
    geodeticLatitude = atan(numerator/denominator);
     
    geodeticLatitude = degrees(geodeticLatitude)
	
    geodeticLongitude = atan(y/x);
	
    geodeticLongitude = degrees(geodeticLongitude)
	
    geodeticLatitudeAux = radians(geodeticLatitude)

    radiusCurvatureFirstVertical = semiMajorAxis/sqrt(1- firstEllipsoidEccentricity**2 *sin(geodeticLatitudeAux)**2)
  
    ellipsoidalAltitude = (sqrt((x**2) + (y**2))/cos(geodeticLatitudeAux)) - radiusCurvatureFirstVertical 
  
    ellipsoidalAltitude = degrees(ellipsoidalAltitude)
  
    return(geodeticLatitude, geodeticLongitude, ellipsoidalAltitude);
  
  def printGoogleFormat(self, latitude, longitude):
	
    hemisphere = 'S';
	
    if latitude > 0: 
      hemisphere = 'N'
		
    degrees = (int)(latitude)
	
    minutes = latitude - degrees
	
    minutesNotRount = minutes * 60
	
    minutes = minutes * 60
    aux = (int)(minutesNotRount)
	
    seconds = minutesNotRount - aux;
    seconds = seconds * 60 
	
    _Latitude = str(degrees) + '°' + str(minutes) + "'" + str(seconds) + "''" + hemisphere
	
    meridian = 'W';
	
    if longitude > 0:
      meridian = 'O'
		
    degrees = (int)(longitude)
	
    minutes = longitude - degrees
	
    minutesNotRount = minutes * 60
	
    minutes = minutes * 60
	
    aux = (int)(minutesNotRount)
	
    seconds = minutesNotRount - aux;
    seconds = seconds * 60	
		
    _Longitude = str(degrees) + '°' + str(minutes) + "'" + str(seconds) + "''" + meridian	
    
    print(_Latitude,_Longitude)
  	
  def CalculateOffset(self, x1, y1, x2, y2):
    if (x2 - x1) == 0:
      raise ValueError('Without solution.')  

    if (randint(0,1)) :
      y3 = y2 - sqrt(((x2 - x1)**2 + (y2 - y1)**2)/(((y2 - y1)/(x2 - x1))**2 + 1))
    else :
      y3 = y2 + sqrt(((x2 - x1)**2 + (y2 - y1)**2)/(((y2 - y1)/(x2 - x1))**2 + 1))

    x3 = x2 + ((y2 - y1)/(x2 - x1) * y2 - ((y2 - y1)/(x2 - x1)) * y3)
    
    return (x3,y3)    
  
  def generateRoute(self):
    
    coordinatesGraphs.GeodesicsToInteger(self)    
    
    index = 0 
    
    while index <= 1:
      
      answer = coordinatesGraphs.convertGeodesicsToCartesian(self,
                                                    self.__numericalLatitude[index], 
                                                    self.__numericalLongitude[index],
                                                    self.__numericalAltitude[index])
                      
      self.__cartesianLatitude[index] = answer[0]
      self.__cartesianLogintude[index] = answer[1]
      self.__cartesianAltitude[index] = answer[2]                                                  
    
      index = index + 1 
      
    detour = coordinatesGraphs.CalculateOffset(self,
                                      self.__cartesianLatitude[0],self.__cartesianLogintude[0],
                                      self.__cartesianLatitude[1],self.__cartesianLogintude[1])  
    
    return coordinatesGraphs.convertCartesianToGeodesics(self, detour[0], detour[1], self.__cartesianAltitude[0])         
    
    
    

    
  
